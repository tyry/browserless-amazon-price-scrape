const puppeteer = require("puppeteer");
const fs = require("fs");

const scrape = async () => {
  const browser = await puppeteer.launch({headless: false});
  // const browser = await puppeteer.connect({ browserWSEndpoint: 'wss://chrome.browserless.io?token=[ADD BROWSERLESS API TOKEN HERE]' })

  const page = await browser.newPage();
  await page.goto("https://www.amazon.com");
  await page.type("#twotabsearchtextbox", "office chair");
  await page.click("#nav-search-submit-button");
  await page.waitForSelector(".s-pagination-next");

  // Go to next results page
  await page.click(".s-pagination-next");
  await page.waitForSelector(".s-pagination-next");

  // Gather product title
  const title = await page.$$eval(
    "h2 span.a-color-base",
    (nodes) => nodes.map((n) => n.innerText)
  );
  // Gather price
  const price = await page.$$eval(
    "[data-component-type='s-search-result'] span.a-price[data-a-color='base'] span.a-offscreen",
    (nodes) => nodes.map((n) => n.innerText)
  );

  // Consolidate product search data
  const amazonSearchArray = title.slice(0, 5).map((value, index) => {
    return {
      title: title[index],
      price: price[index],
    };
  });
  const jsonData = JSON.stringify(amazonSearchArray, null, 2);
  fs.writeFileSync("amazonSearchResults.json", jsonData);
  await browser.close();
};
scrape();
